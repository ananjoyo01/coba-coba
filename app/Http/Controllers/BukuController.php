<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BukuController extends Controller
{
    public function databuku(){
        $buku = DB::table('buku')->get();
        return view('buku',['data_buku'=>$buku]);
    }

    public function tambahbuku(){
        return view('tambah_buku');
    }

    public function prosesTambahbuku(Request $request){
        DB::table('buku')->insert([
            'judul_buku'=>$request->judul_buku,
            'pengarang'=>$request->pengarang,
            'penerbit'=>$request->penerbit,
            'tahun_terbit'=>$request->tahun_terbit,
            'tebal'=>$request->tebal,
            'isbn'=>$request->isbn,
            'stok_buku'=>$request->stok_buku,
            'biaya_sewa_harian'=>$request->biaya_sewa_harian
        ]);

        return redirect('/buku');
    }

    public function getId ($id){
        $buku = DB::table('buku')->where('id_buku',$id)->get();
        return view('edit_buku',['data_buku'=>$buku]);
    }

    public function prosesEditbuku(Request $request){
        DB::table('buku')->where('id_buku',$request->id)->update([
            'judul_buku'=>$request->judul_buku,
            'pengarang'=>$request->pengarang,
            'penerbit'=>$request->penerbit,
            'tahun_terbit'=>$request->tahun_terbit,
            'tebal'=>$request->tebal,
            'isbn'=>$request->isbn,
            'stok_buku'=>$request->stok_buku,
            'biaya_sewa_harian'=>$request->biaya_sewa_harian
        ]);

        return redirect('/buku');
    }

    public function prosesHapus($id){
        DB::table('buku')->where('id_buku',$id)->delete();
        return redirect('/buku');
    }
}
