<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    public function datamahasiswa(){
        $mhs = DB::table('mahasiswa')->get();
        return view('mahasiswa',['data_maha'=>$mhs]);
    }

    public function tambahmahasiswa(){
        return view('tambah_maha');
    }

    public function prosesTambahmaha(Request $request){
        DB::table('mahasiswa')->insert([
            'nama'=>$request->nama,
            'nim'=>$request->nim,
            'email'=>$request->email,
            'no_telp'=>$request->no_telp,
            'prodi'=>$request->prodi,
            'jurusan'=>$request->jurusan,
            'fakultas'=>$request->fakultas
        ]);

        return redirect('/');
    }

    public function getId ($id){
        $mhs = DB::table('mahasiswa')->where('id_mahasiswa',$id)->get();
        return view('edit_maha',['data_maha'=>$mhs]);
    }

    public function prosesEditmaha(Request $request){
        DB::table('mahasiswa')->where('id_mahasiswa',$request->id)->update([
            'nama'=>$request->nama,
            'nim'=>$request->nim,
            'email'=>$request->email,
            'no_telp'=>$request->no_telp,
            'prodi'=>$request->prodi,
            'jurusan'=>$request->jurusan,
            'fakultas'=>$request->fakultas
        ]);

        return redirect('/');
    }

    public function prosesHapus($id){
        DB::table('mahasiswa')->where('id_mahasiswa',$id)->delete();
        return redirect('/');
    }
}
