<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransaksiController extends Controller
{
    public function datatransaksi(){
        $transaksi = DB::table('transaksi')->get();
        return view('transaksi',['data_transaksi'=>$transaksi]);
    }

    public function tambahtransaksi(){
        return view('tambah_transaksi');
    }

    public function prosesTambahtransaksi(Request $request){
        DB::table('transaksi')->insert([
            'id_mahasiswa'=>$request->id_mahasiswa,
            'id_buku'=>$request->id_buku,
            'tanggal_pinjam'=>$request->tanggal_pinjam,
            'tanggal_kembali'=>$request->anggal_kembali,
            'status_pinjam'=>$request->status_pinjam,
            'total_biaya'=>$request->total_biaya
        ]);

        return redirect('/transaksi');
    }

    public function getId ($id){
        $transaksi = DB::table('transaksi')->where('id_transaksi',$id)->get();
        return view('edit_transaksi',['data_transaksi'=>$transaksi]);
    }

    public function prosesEdittransaksi(Request $request){
        DB::table('transaksi')->where('id_transaksi',$request->id)->update([
            'id_mahasiswa'=>$request->id_mahasiswa,
            'id_buku'=>$request->id_buku,
            'tanggal_pinjam'=>$request->tanggal_pinjam,
            'tanggal_kembali'=>$request->anggal_kembali,
            'status_pinjam'=>$request->status_pinjam,
            'total_biaya'=>$request->total_biaya
        ]);

        return redirect('/transaksi');
    }

    public function prosesHapus($id){
        DB::table('transaksi')->where('id_transaksi',$id)->delete();
        return redirect('/transaksi');
    }
}
