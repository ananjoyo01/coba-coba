<?php

use Illuminate\Support\Facades\Route;


Route::get('/', [App\Http\Controllers\MahasiswaController::class,'datamahasiswa'])->name('data_maha');

Route::get('/buku', [App\Http\Controllers\BukuController::class,'databuku'])->name('data_buku');

Route::get('/transaksi', [App\Http\Controllers\TransaksiController::class,'datatransaksi'])->name('data_transaksi');

Route::get('/tambah_maha', [App\Http\Controllers\MahasiswaController::class,'tambahmahasiswa'])->name('tambah_maha');

Route::get('/tambah_buku', [App\Http\Controllers\BukuController::class,'tambahbuku'])->name('tambah_buku');

Route::get('/tambah_transaksi', [App\Http\Controllers\TransaksiController::class,'tambahtransaksi'])->name('tambah_transaksi');

Route::post('/prosesTambahmaha', [App\Http\Controllers\MahasiswaController::class,'prosesTambahmaha'])->name('tambah');

Route::post('/prosesTambahbuku', [App\Http\Controllers\BukuController::class,'prosesTambahbuku'])->name('tambah');

Route::post('/prosesTambahtransaksi', [App\Http\Controllers\TransaksiController::class,'prosesTambahtransaksi'])->name('tambah');

Route::get('/getId{id_mahasiswa}', [App\Http\Controllers\MahasiswaController::class,'getId'])->name('edit_maha');

Route::get('/getId{id_buku}', [App\Http\Controllers\BukuController::class,'getId'])->name('edit_buku');

Route::get('/getId{id_transaksi}', [App\Http\Controllers\TransaksiController::class,'getId'])->name('edit_transaksi');

Route::post('/prosesEditmaha', [App\Http\Controllers\MahasiswaController::class,'prosesEditmaha'])->name('edit');

Route::post('/prosesEditbuku', [App\Http\Controllers\BukuController::class,'prosesEditbuku'])->name('edit');

Route::post('/prosesEdittransaksi', [App\Http\Controllers\TransaksiController::class,'prosesEdittransaksi'])->name('edit');

Route::get('/prosesHapus{id_mahasiswa}', [App\Http\Controllers\MahasiswaController::class,'prosesHapus']);

Route::get('/prosesHapus{id_buku}', [App\Http\Controllers\BukuController::class,'prosesHapus']);

Route::get('/prosesHapus{id_transaksi}', [App\Http\Controllers\TransaksiController::class,'prosesHapus']);

