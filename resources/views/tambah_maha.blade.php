@extends('layouts.main')

@section('nav_right')
    <li><a href="{{ route('data_maha') }}">Data Mahasiswa</a></li>
    <li class="active">Tambah Mahasiswa</li>
@endsection

@section('konten')

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <strong>Tambah</strong> Data Mahasiswa
          </div>
          <div class="card-body card-block">
            <form action="{{ route('tambah_maha') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field() }}
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nama </label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="nama" placeholder="Nama Mahasiswa" class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">NIM</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="nim" placeholder="NIM Mahasiswa" class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Email</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="email" placeholder="Email Mahasiswa" class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">No Telp</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="no_telp" placeholder="No Telp Mahasiswa" class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Prodi</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="prodi" placeholder="Prodi Mahasiswa" class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Jurusan</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="jurusan" placeholder="Jurusan Mahasiswa" class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Fakultas</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="fakultas" placeholder="Fakultas Mahasiswa" class="form-control"></div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                    <button type="reset" class="btn btn-danger btn-sm">Batal</button>
                </div>
            </form>
        </div>
    </div>
@endsection
