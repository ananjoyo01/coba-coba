@extends('layouts.main')

@section('nav_right')
    <li><a href="{{ route('data_maha') }}">Data Mahasiswa</a></li>
    <li class="active">Edit Karyawan</li>
@endsection

@section('konten')

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <strong>Edit</strong> Data Mahasiswa
          </div>
          <div class="card-body card-block">
            @foreach ($data_maha as $data)
                <form action="{{ route('edit') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }} 
                    <input type="hidden" name="id" value="{{ $data->id_mahasiswa }}">
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nama</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="nama" placeholder="Nama Mahasiswa" class="form-control" value="{{ $data->nama }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">NIM</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="nim" placeholder="NIM" class="form-control" value="{{ $data->nim }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Email</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="email" placeholder="Email" class="form-control" value="{{ $data->email }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">No Telepon</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="no_telp" placeholder="No Telepon" class="form-control"  value="{{ $data->no_telp }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Prodi</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="prodi" placeholder="Prodi" class="form-control" value="{{ $data->prodi }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Jurusan</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="jurusan" placeholder="Jurusan" class="form-control" value="{{ $data->jurusan }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Fakultas</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="fakultas" placeholder="Prodi" class="form-control" value="{{ $data->fakultas }}"></div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                        <button type="reset" class="btn btn-danger btn-sm">Batal</button>
                    </div>
                </form>
            @endforeach
        </div>
    </div>
@endsection
