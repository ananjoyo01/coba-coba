@extends('layouts.main')

@section('nav_right')
    <li class="active">Data mahasiswa</li>
@endsection

@section('konten')

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Data Mahasiswa</strong>
                    </div>
                    <div class="card-body">
                        <a href="{{ route('tambah_maha') }}"><button type="button" class="btn btn-success">Tambah mahasiswa</button></a>
                    </div>
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th>NIM</th>
                    <th>Email</th>
                    <th>No Telp</th>
                    <th>Prodi</th>
                    <th>Jurusan</th>
                    <th>Fakultas</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data_maha as $data)
                        <tr>
                            <td>{{ $data->nama }}</td>
                            <td>{{ $data->nim }}</td>
                            <td>{{ $data->email }}</td>
                            <td>{{ $data->no_telp }}</td>
                            <td>{{ $data->prodi }}</td>
                            <td>{{ $data->jurusan }}</td>
                            <td>{{ $data->fakultas }}</td>
                            <td>
                                <a href="{{ route('edit_maha', [$data->id_mahasiswa]) }}"><button type="button" class="btn btn-primary btn-sm" style="color:white">Edit</button></a>
                                <a href='/prosesHapus/{id_mahasiswa}'><button type="button" class="btn btn-danger btn-sm">Hapus</button></a>
                            </td>
                        </tr>
                    @endforeach
                </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    
@endsection

