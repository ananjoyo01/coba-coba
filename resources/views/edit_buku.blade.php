@extends('layouts.main')

@section('nav_right')
    <li><a href="{{ route('data_buku') }}">Data Buku</a></li>
    <li class="active">Edit Buku</li>
@endsection

@section('konten')

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <strong>Edit</strong> Data Buku
          </div>
          <div class="card-body card-block">
            @foreach ($data_buku as $data)
                <form action="{{ route('edit') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $data->id_buku }}">
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Judul Buku</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name=judul_buku" placeholder="Judul Buku" class="form-control" value="{{ $data->judul_buku }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Pengarang</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="pengarang" placeholder="Pengarang" class="form-control" value="{{ $data->pengarang}}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Penerbit</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="penerbit" placeholder="Penerbit" class="form-control" value="{{ $data->penerbit }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Tahun Terbit</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="tahun_terbit" placeholder="Tahun Terbit" class="form-control" value="{{ $data-tahun_terbit }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Tebal</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="tebal" placeholder="Tebal" class="form-control"  value="{{ $data->tebal }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">ISBN</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="isbn" placeholder="ISBN" class="form-control" value="{{ $data->isbn }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Stok Buku</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="stok_buku" placeholder="Stok Buku" class="form-control" value="{{ $data->stok_buku }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Biaya Sewa Harian</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="biaya_sewa_harian" placeholder="Biaya Sewa Harian" class="form-control" value="{{ $data->biaya_sewa_harian }}"></div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                        <button type="reset" class="btn btn-danger btn-sm">Batal</button>
                    </div>
                </form>
            @endforeach
        </div>
    </div>
@endsection
