@extends('layouts.main')

@section('nav_right')
    <li class="active">Data Buku</li>
@endsection

@section('konten')

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Data Buku</strong>
                    </div>
                    <div class="card-body">
                        <a href="{{ route('tambah_buku') }}"><button type="button" class="btn btn-success">Tambah buku</button></a>
                    </div>
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>Judul Buku</th>
                    <th>Pengarang</th>
                    <th>Penerbit</th>
                    <th>Tahun Terbit</th>
                    <th>Tebal</th>
                    <th>ISBN</th>
                    <th>Stok Buku</th>
                    <th>Biaya Sewa Harian</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data_buku as $data)
                        <tr>
                            <td>{{ $data->judul_buku }}</td>
                            <td>{{ $data->pengarang }}</td>
                            <td>{{ $data->penerbit }}</td>
                            <td>{{ $data->tahun_terbit }}</td>
                            <td>{{ $data->tebal }}</td>
                            <td>{{ $data->isbn }}</td>
                            <td>{{ $data->stok_buku }}</td>
                            <td>{{ $data->biaya_sewa_harian }}</td>
                            <td>
                                <a href="{{ route('edit_buku', [$data->id_buku]) }}"><button type="button" class="btn btn-primary btn-sm" style="color:white">Edit</button></a>
                                <a href='/prosesHapus/{id_buku}'><button type="button" class="btn btn-danger btn-sm">Hapus</button></a>
                            </td>
                        </tr>
                    @endforeach
                </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>

@endsection

