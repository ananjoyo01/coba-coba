@extends('layouts.main')

@section('nav_right')
    <li><a href="{{ route('data_transaksi') }}">Data Transaksi</a></li>
    <li class="active">Tambah Transaksi</li>
@endsection

@section('konten')

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <strong>Tambah</strong> Data Transaksi
          </div>
          <div class="card-body card-block">
            <form action="{{ route('tambah_transaksi') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field() }}
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Id Mahasiswa</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="id_mahasiswa" placeholder="Id Mahasiswa" class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Id Buku</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="id_buku" placeholder="Id Buku" class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Tanggal Pinjam</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="tanggal_pinjam" placeholder="Tanggal Pinjam" class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Tanggal Kembali</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="tanggal_kembali" placeholder="Tanggal Kembali" class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Status Pinjam</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="status_pinjam" placeholder="Status Pinjam" class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Total Biaya</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="total_biaya" placeholder="Total Biaya" class="form-control"></div>
                </div>


                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                    <button type="reset" class="btn btn-danger btn-sm">Batal</button>
                </div>
            </form>
        </div>
    </div>
@endsection
