@extends('layouts.main')

@section('nav_right')
    <li class="active">Data Transaksi</li>
@endsection

@section('konten')

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Data Transaksi</strong>
                    </div>
                    <div class="card-body">
                        <a href="{{ route('tambah_transaksi') }}"><button type="button" class="btn btn-success">Tambah transaksi</button></a>
                    </div>
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>Id Mahasiswa</th>
                    <th>Id Buku</th>
                    <th>Tanggal Pinjam</th>
                    <th>Tanggal Kembali</th>
                    <th>Status Pinjam</th>
                    <th>Total Biaya</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data_transaksi as $data)
                        <tr>
                            <td>{{ $data->id_mahasiswa }}</td>
                            <td>{{ $data->id_buku }}</td>
                            <td>{{ $data->tanggal_pinjam }}</td>
                            <td>{{ $data->tanggal_kembali }}</td>
                            <td>{{ $data->status_pinjam }}</td>
                            <td>{{ $data->total_biaya}}</td>
                            <td>
                                <a href="{{ route('edit_transaksi', [$data->id_transaksi]) }}"><button type="button" class="btn btn-primary btn-sm" style="color:white">Edit</button></a>
                                <a href='/prosesHapus/{id_transaksi}'><button type="button" class="btn btn-danger btn-sm">Hapus</button></a>
                            </td>
                        </tr>
                    @endforeach
                </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>

@endsection

